package ldh.maker.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import ldh.maker.constants.TreeNodeTypeEnum;
import ldh.maker.db.TreeNodeDb;
import ldh.maker.handle.ProjectTreeHandle;
import ldh.maker.handle.TreeContextMenuHandle;
import ldh.maker.util.UiUtil;
import ldh.maker.vo.TreeNode;
import org.controlsfx.control.StatusBar;

public class MainController implements Initializable {

    @FXML
    TreeView<TreeNode> projectTree;
    @FXML
    TabPane contentPane;
    @FXML
    ContextMenu treeContextMenu;
    @FXML
    StatusBar statusBar;

    public static TreeView<TreeNode> PROJECT_TREE;

    public static final TreeItem<TreeNode> root = new TreeItem<>(new TreeNode(TreeNodeTypeEnum.ROOT, "root", null));

    @FXML
    public void createProject(){
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("创建构建计划");
        dialog.setHeaderText("创建构建计划");
        dialog.setContentText("构建计划名称");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            try {
                TreeNode projectTreeData = new TreeNode(TreeNodeTypeEnum.PROJECT, result.get(), root.getValue());
                projectTreeData.setData(result.get());
                TreeNodeDb.save(projectTreeData);
                TreeItem<TreeNode> project = new TreeItem<>(projectTreeData);

                TreeNode dbData = new TreeNode(TreeNodeTypeEnum.DATABASE, "数据库", projectTreeData);
                dbData.setData("数据库");
                TreeNodeDb.save(dbData);
                TreeItem<TreeNode> db = new TreeItem<>(dbData);

                TreeNode codeData = new TreeNode(TreeNodeTypeEnum.CODE, "生成代码", projectTreeData);
                codeData.setData("生成代码");
                TreeNodeDb.save(codeData);
                TreeItem<TreeNode> code = new TreeItem<>(codeData);

                project.getChildren().addAll(db, code);
                projectTreeData.getChildren().add(dbData);
                projectTreeData.getChildren().add(codeData);
                root.getChildren().add(project);
                project.setExpanded(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void createCode() {

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        UiUtil.TREE = projectTree;
        projectTree.setRoot(root);
        projectTree.setShowRoot(false);

        new ProjectTreeHandle(projectTree, contentPane);
        new TreeContextMenuHandle(treeContextMenu, projectTree, this);

        Task<Void> initDb = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                TreeNodeDb.initProject(projectTree);
                return null;
            }
        };
//        new Thread(initDb).start();
        Platform.runLater(initDb);

        PROJECT_TREE = projectTree;

        UiUtil.STATUSBAR = statusBar;
    }

    public TabPane getContentPane() {
        return contentPane;
    }

}
