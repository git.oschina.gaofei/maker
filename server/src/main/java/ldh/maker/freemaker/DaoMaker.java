package ldh.maker.freemaker;

import ldh.bean.util.BeanInfoUtil;
import ldh.common.json.ValuedEnumObjectSerializer;
import ldh.database.Column;
import ldh.database.UniqueIndex;
import ldh.maker.util.FreeMakerUtil;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public class DaoMaker extends BeanMaker<DaoMaker> {
	
	private Class<?> bean;
	private BeanMaker<?> beanMaker;
	
	public DaoMaker() {
		super();
		this.imports(List.class);
		
		
	}
	public DaoMaker bean(Class<?> bean) {
		this.bean = bean;
		this.imports.add(bean.getName());
		return this;
	}
	
	public DaoMaker beanMaker(BeanMaker<?> beanMaker) {
		this.beanMaker = beanMaker;
		this.imports.add(beanMaker.getName());
		return this;
	}
	
	public void data() {
		check();
		Set<UniqueIndex> uis = this.table.getIndexies();
		if (uis != null) {
			for (UniqueIndex ui : uis) {
				for (Column c : ui.getColumns()) {
					if (!BeanInfoUtil.isBaseClass(c.getPropertyClass())) {
						this.imports(c.getPropertyClass());
					}
				}
			}
		}
		for (UniqueIndex ui : table.getIndexies()) {
			if (ui.getColumnNames().size() > 1) {
				this.imports(Param.class);
				break;
			}
		}

		String root = pack.substring(0, pack.lastIndexOf("."));
		for (Column column : table.getColumnList()) {
			if (FreeMakerUtil.isEnum(column)) {
				imports.add(root + ".constant." + column.getJavaType());
				imports.add(ValuedEnumObjectSerializer.class.getName());
			}
		}
		
		if (bean != null) {
			data.put("bean", bean.getSimpleName());
			data.put("beanWhere", bean.getSimpleName() + "Where");
		}
		if (beanMaker != null){
			data.put("bean", beanMaker.getSimpleName());
			data.put("beanWhere", beanMaker.getSimpleName() + "Where");
		}
		
		super.data();
	}
	
	public void check() {
		if (bean == null && beanMaker == null) {
			throw new NullPointerException("bean or beanMaker is not null!!!");
		}
		super.check();
	}
	
	@Override
	public DaoMaker make() {
		data();
		out("dao.ftl", data);
		
		return this;
	}
	
	public static void main(String[] args) {
		String outPath = "E:\\project\\eclipse\\datacenter\\website_statistics\\admin\\src\\main\\base\\ldh\\base\\make\\freemaker";
		
		new DaoMaker()
		 	.pack("ldh.base.make.freemaker")
		 	.outPath(outPath)
		 	.className("ScenicDao")
		 	.make();
		
	}

	
}
