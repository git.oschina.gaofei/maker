# 代码生成器

采用javafx制作UI界面，通过操作界面，直接生产可以运行的代码，
 对表生成了增，删，改，查等功能，还生成了相应的界面页面。

## 生成代码说明：
    根据数据库结构，按照用户的要求，动态生成可执行代码。
    生成的maven结构。spring mvc + spring + mybatis传统接口
    前端多技术： easyui, bootstrap + jsp, bootstrap + freemarker, javafx ui


子项目说明：
## core 项目
    所有子项目的基础，包含了对数据库结构的描述。
## server 项目
    通过spring boot 生成微服务接口，包含了数据的增删改查的json接口
## web项目
    1，可以生成easyui前端界面
    2，可以生成bootstrap + jsp前端界面
    3，可以生成bootstrap + freemarker 前端界面
    4，支持vertx结构
## desktop
    1，可以生成javafx客户端桌面应用
    2，可以生成javafx+spring+mybatis桌面应用
## mobile
    暂时还不支持
## android
    暂时还不支持

# 功能列表
    1, spring boot + spring + mybatis + mysql + 各种前端
    2, 支持mybatis一对一，一对多，多对多关系查询，并支持动态选择是否生成
    3, 支持mybatis分页查询，唯一索引删除，查询，更新等
    4, 支持表中字段枚举生成
    5, 支持表中字段重注释，并且同步到POJO的注释上
    6, 支持对表筛选，筛选中的表可以不生成对应的pojo + mybatis mapper + dao + service + controller;
    7, 表对应的前端可以动态控制，控制只显示需要的字段
    8, 支持Swagger2
    9, 支持vertx + sync + quasar + freemarker

# 操作图
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/0.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/1.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/2.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/3.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/4.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/5.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/6.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/7.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/8.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/9.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/10.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/10-1.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/10-2.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/11.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/12.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/13.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/14.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/15.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/16.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/17.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/18.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/19.png)
![Alt text](https://gitee.com/ldh123/maker/raw/master/doc/images/20.png)